import * as CryptoJS from "crypto-js";

class Block {
    public index: number;
    public hash: string;
    public previousHash: string;
    public data: string;
    public timestamp: number;

    static calculateBlockHash = (
        index : number,
        previousHash : string,
        timestamp : number,
        data : string
    ) : string => {
        return CryptoJS.SHA256(index + previousHash + timestamp + data).toString();
    };

    constructor(
        index: number,
        hash: string,
        previousHash: string,
        data: string,
        timestamp: number
    ) {
        this.index = index;
        this.hash = hash;
        this.previousHash = previousHash;
        this.data = data;
        this.timestamp = timestamp;
    }

    static validateStructure = (block : Block) : boolean =>
        typeof block.index === "number" &&
        typeof block.hash === "string"&&
        typeof block.previousHash === "string" &&
        typeof block.timestamp === "number" &&
        typeof block.data === "string";
}

const genesisBlock : Block = new Block(0, "lksdjfsdfi!#123123", "", "genesis",123456);
let blockchain : Block[] = [genesisBlock];

const getBlockChain = () : Block[] => blockchain;
const getLatestBlock = () : Block => blockchain[blockchain.length - 1];
const getNewTimestamp = () : number => Math.round(new Date().getTime() / 1000);
const createNewBlock = (data : string) : Block => {
    const previousBlock : Block = getLatestBlock();
    const newIndex : number = previousBlock.index + 1;
    const newTimestamp : number = getNewTimestamp();
    const newHash : string = Block.calculateBlockHash(newIndex, previousBlock.hash, newTimestamp, data);

    const newBlock = new Block(newIndex, newHash, previousBlock.hash, data, newTimestamp);
    addBlock(newBlock);

    return newBlock;
};

const isBlockValid = (candidateBlock : Block) : boolean => {
    const previousBlock = getLatestBlock();
    return Block.validateStructure(candidateBlock) &&
        candidateBlock.index === previousBlock.index + 1 &&
        candidateBlock.previousHash === previousBlock.hash &&
        Block.calculateBlockHash(
            candidateBlock.index,
            previousBlock.hash,
            candidateBlock.timestamp,
            candidateBlock.data) === candidateBlock.hash;
};

const addBlock = (candidateBlock : Block) : void => {
    if (isBlockValid(candidateBlock)) {
        blockchain.push(candidateBlock);
    }
};

const testBlockchain = () => {
    createNewBlock("second block");
    createNewBlock("third block");
    createNewBlock("fourth block");
    console.log(blockchain);
};

testBlockchain();

export {};