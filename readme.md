# type chain.

### 20181022
### nomad coders.

# intro and what are we building.

* typescript - superset of javascript.
* javascript 는 팀으로 일할 때는 별로 좋지 않다.
* 또한, 버그를 최대한 줄여야 한다.
* 이럴 때 타입스크립트가 좋다.

# setting typescript up.

* `yarn global add typescript`
* `tsconfig.json` 을 만든다.

~~~json
// tsconfig.json
{  
  "compilerOptions" : {  
    "module" : "commonjs",  
  "target" : "ES2015",  
  "sourceMap": true  
  },  
  "include" : ["index.ts"],  
  "exclude": ["node_modules"]  
}
~~~

---

* `tsc` 로 컴파일.
* `package.json` 에 실행 스크립트를 추가한다.

~~~json
...
"scripts" : {  
  "start" : "node index.js",  
  "prestart" : "tsc" // 자동으로 start 전에 실행된다.
}
~~~

* compilerOptions - outDir 등을 세팅할 수 있다.

# first steps with typescript.

* typescript 는, 아규먼트의 개수, 아규먼트의 타입을 모두 체크해준다.

~~~typescript
const sayHi = (name, age, gender) => {  
    console.log(`Hello ${name}, you are ${age}, you are a ${gender}`);  
};  
  
sayHi(name, age); // 여기에서 에러 메시지를 보여준다. 일반 자바스크립트에서는 보여주지 않을 것.
~~~

* 그러나, argument 뒤에 `?`를 붙임으로써 파라미터가 optional 임을 지정할 수 있다.

~~~typescript
const sayHi = (name, age, gender?) => {  
    console.log(`Hello ${name}, you are ${age}, you are a ${gender}`);  
};

sayHi(name, age);
~~~

# types in typescript.

* type을 직접 지정할 수 있다.
* vscode 를 쓰면, typescript integration 이 되어서 편하게 사용할 수 있다.

~~~typescript
const sayHi = (name : string, age : number, gender : string) => {  
    console.log(`Hello ${name}, you are ${age}, you are a ${gender}`);  
};
~~~

* 어떤 함수에서 아무것도 리턴하지 않으면, 리턴값이 void 로 지정된다.
* 리턴타입을 명시하고 싶다면, 뒤에 리턴 타입을 써준다.

~~~typescript
const sayHi = (name : string, age : number, gender : string) : void => {  
    console.log(`Hello ${name}, you are ${age}, you are a ${gender}`);  
};
~~~

* 리턴 타입을 써줬는데 void 가 아니면서 아무것도 리턴하지 않으면 에러가 난다.

## directory 재지정.

* `yarn add tsc-watch` 를 추가한다. - 파일을 변경하면 자동으로 반영된다.
* src, dist 폴더를 만든다.
* tsconfig.json 에서 `"include" : ["index.ts"],` 를 `"include" : ["src/**/*"],`로 바꾼다.
* `index.ts` 를 src 폴더 안으로 옮긴다.
* `"ourDir" : "dist"` 를 `tsconfig.json` 안에 추가한다.
* 스크립트로 `"dev" : "tsc-watch --onSuccess \" node dist/index.js \" "` 를 추가.
* `.gitignore` 에 dist 추가.

## issue.

### `Cannot find module 'typescript/bin/tsc'` 에러

* `yarn add typescript`로, 프로젝트 내부에 타입스크립트를 깔았더니 해결된다.

# interface on typescript.

* 함수에 객체를 넘겨야 할 때는 interface 를 만든다.
* 어떤 객체의 타입을 미리 정해놓을 수 있다.

~~~typescript
interface Human {  
    name : string,  
  age : number,  
  gender : string  
}

const sayHiInterface = (person : Human) : string => {  
    return `Hello ${person.name}, you are ${person.age}, you are a ${person.gender}`;  
};

console.log(sayHiInterface({  
    name : "kim",  
  age : 44,  
  gender : "female"  
}));
~~~

* interface 를 정해놓으면, 어떤 property 가 정해져 있는지 알 수 있으므로 굉장히 편하다.

~~~typescript
// destructuring 가능하다.

const sayHiInterface = ({name, age, gender} : Human) : string => {  
    return `Hello ${name}, you are ${age}, you are a ${gender}`;  
};
~~~

# classes on typescript - part one.

* interface 는 자바스크립트로 가지 않는다.
* 따라서, 클래스를 쓰는 게 좋다?

~~~typescript
class Human {  
  public name : string;  
  public age : number;  
  public gender : string;  
  
  constructor(name : string, age : number, gender : string) { 
        this.name = name;  
  this.age = age;  
  this.gender = gender;  
  }  
}

const lynn = new Human("lynn", 25, "female");
console.log(sayHiInterface(lynn));
~~~

* `  constructor(name : string, age : number, gender? : string) {` 면, gender 는 optional이 된다.

---

* react 등과 같이 쓰려면 interface 보다는 class 가 호환적인 측면에서 낫다.
* public, private, protected 를 지정할 수 있지만, 자바스크립트에서는 안보인다.
* 변수를 private 으로 지정하면 접근 제한이 걸린다.
* private 변수를 쓰려면, 앞에 _를 붙여서 이름에 중복을 방지한다.

~~~typescript
class Human {  
    public name : string;  
  private readonly _age : number; 
  public gender : string;  
  
  constructor(name : string, age : number, gender : string) {  
        this.name = name;  
  this._age = age;  
  this.gender = gender;  
  }  
  
    get age() {  
        return this._age;  
  }  
}
~~~

* `readonly` 를 앞에 붙이면, 정말 read only 가 된다.

# blockchain creating a block.

* 블록 클래스를 정의하고 블록체인을 만든다.

~~~typescript
class Block {  
    public index: number;  
  public hash: string;  
  public previousHash: string;  
  public data: string;  
  public timestamp: number;  
  
  constructor(index: number, hash: string, previousHash: string, data: string, timestamp: number) {  
        this.index = index;  
  this.hash = hash;  
  this.previousHash = previousHash;  
  this.data = data;  
  this.timestamp = timestamp;  
  }  
}  
  
const genesisBlock : Block = new Block(0, "lksdjfsdfi!#123123", "", "genesis",123456);  
  
let blockchain : [Block] = [genesisBlock];  
  
blockchain.push({  
    index : 123,  
  hash : "h",  
  previousHash : "h",  
  data : "d",  
  timestamp : 123  
});  // 이렇게도 가능하다.
  
console.log(blockchain);  
  
export {};
~~~

# creating a block part two.

* hash 는 모든 데이터를 합해서 만든 하나의 스트링이다.
* `yarn add crypto-js`로 설치.
* `import * as CryptoJS from "crypto-js";`로 임포트한다.

---

* static 메소드를 만들어서, 언제나 접근할 수 있게 한다.
* `let blockchain : Block[] = [genesisBlock];` 이렇게 배열을 리턴하는 타입을 쓴다.

~~~typescript
static calculateBlockHash = (  
    index : number,  
  previousHash : string,  
  timestamp : number,  
  data : string  
) : string => {  
    return CryptoJS.SHA256(index + previousHash + timestamp + data).toString();  
};

const getBlockChain = () : Block[] => blockchain;  
const getLatestBlock = () : Block => blockchain[blockchain.length - 1];  
const getNewTimestamp = () : number => Math.round(new Date().getTime() / 1000);
~~~

# creating a block part three.

* block 을 새로 생성하는 함수를 만들고, 블록체인에 넣어본다.

~~~typescript
const createNewBlock = (data : string) : Block => {  
    const previousBlock : Block = getLatestBlock();  
  const newIndex : number = previousBlock.index + 1;  
  const newTimestamp : number = getNewTimestamp();  
  const newHash : string = Block.calculateBlockHash(newIndex, previousBlock.hash, newTimestamp, data);  
  
  return new Block(newIndex, newHash, previousBlock.hash, data, newTimestamp);  
};  
  
console.log(blockchain);  
  
blockchain.push(createNewBlock("hello"));  
blockchain.push(createNewBlock("bye bye"));  
console.log(blockchain);
~~~

# validating block structure.

* 어떤 block 을 넘기면, 그 블록이 정상인지 체크하는 함수를 작성한다.
* 약간 테스트코드 만드는 느낌이다.

~~~typescript

class Block { ...
static validateStructure = (block : Block) : boolean =>  
    typeof block.index === "number" &&  
    typeof block.hash === "string"&&  
    typeof block.previousHash === "string" &&  
    typeof block.timestamp === "number" &&  
    typeof block.data === "string";

const isBlockValid = (candidateBlock : Block) : boolean => {  
    const previousBlock = blockchain[candidateBlock.index - 1];  
  return Block.validateStructure(candidateBlock) &&  
        candidateBlock.index === previousBlock.index + 1 &&  
        candidateBlock.previousHash === previousBlock.hash &&  
        Block.calculateBlockHash(  
            candidateBlock.index,  
  previousBlock.hash,  
  candidateBlock.timestamp,  
  candidateBlock.data) === candidateBlock.hash;  
};
~~~

# validating block structure part two.

* addBlock() 을 작성한다.

~~~typescript
const addBlock = (candidateBlock : Block) : void => {  
    if (isBlockValid(candidateBlock)) {  
        blockchain.push(candidateBlock);  
  }  
};
~~~

# conclusions.

* addBlock 을 createNewBlock 안에 쓴다.

~~~typescript
const createNewBlock = (data : string) : Block => {  
    const previousBlock : Block = getLatestBlock();  
  const newIndex : number = previousBlock.index + 1;  
  const newTimestamp : number = getNewTimestamp();  
  const newHash : string = Block.calculateBlockHash(newIndex, previousBlock.hash, newTimestamp, data);  
  
  const newBlock = new Block(newIndex, newHash, previousBlock.hash, data, newTimestamp);  
  addBlock(newBlock);  
  
  return newBlock;  
};
~~~





























